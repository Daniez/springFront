import { UserModel } from './../model/user.model';
import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';


import { UserService } from './user.service';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers:[UserService]
})
export class UserComponent implements OnInit {

  //array to list users
  private listUsers : Array<UserModel>; 
  constructor(private userService: UserService , private router : Router) { }

  ngOnInit() {
    this.loadAllUsers();
  }

  private loadAllUsers():void {
    this.userService.getUserList().subscribe(res =>{
      this.listUsers = res;
      console.log(res);
    });
    
  }

  private editUser(user:UserModel):void {
    sessionStorage.setItem("user",  JSON.stringify(user));
    this.router.navigate(["/userCreateComponent"]);
  }
}
