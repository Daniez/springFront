import { ParentEntity } from './parentEntity.model';
export class UserModel extends ParentEntity{
    public document: number;
    public secondName: string;
    public email: string;
    public cellPhone: string;
    public password: string;
}