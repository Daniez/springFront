import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { patch } from 'webdriver-js-extender';
import { UserComponent } from './user/user.component';
import { UserCreateComponent } from './user-create/user-create.component';


const routes: Routes = [
  { path: '', redirectTo: '/userComponent', pathMatch: 'full' },
  { path: 'appComponent', component: AppComponent },
  { path: 'userComponent', component: UserComponent },
  { path: 'userCreateComponent', component: UserCreateComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
