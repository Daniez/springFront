import { Router} from '@angular/router';
import { Component, OnInit } from '@angular/core';


import { UserCreateService } from './user-create.service';
import { UserModel } from '../model/user.model';
import { RestResponse } from './../model/restResponse.model';
import { OK } from '../model/httpStatus.model';
@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css'],
  providers: [UserCreateService]
})
export class UserCreateComponent implements OnInit {
  private user: UserModel;
  private isValid: boolean = true;
  private message: string = ""; 

  constructor(private userCreateService:UserCreateService , private router : Router) { 
    this.user = new UserModel;

    if(sessionStorage.getItem("user")){
      this.user = JSON.parse(sessionStorage.getItem("user"));
    }else {
      this.user = new UserModel;
    }
  }

  ngOnInit() {
  }
  /**
   * Methot to save or edit users 
   * 
   */
  public saveOrEdit():void{
    this.isValid = this.userCreateService.validateUser(this.user);
    if(this.isValid){
      this.userCreateService.saveOrEdit(this.user).subscribe(res => {
        if(res.responseCode == OK){
          this.router.navigate(["/userComponent"]);
        }else{
          
          this.isValid = false;
          this.message == res.message;
        }
      });
    }else {
      this.message ="Error los campos deben ser obligatorios";
    }

  }

}
