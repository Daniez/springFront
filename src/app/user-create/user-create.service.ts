import { UserModel } from './../model/user.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RestResponse } from '../model/restResponse.model';
import { Observable } from 'rxjs/Observable';
import { useAnimation } from '@angular/core/src/animation/dsl';

@Injectable()
export class UserCreateService {

  constructor(private http:HttpClient) { }

  public validateUser(user:UserModel): boolean{
    let isValidate = true;
    if(user.document == null){
      isValidate = false;
    }else if(!user.name){
      isValidate = false;
    }else if (!user.secondName){
      isValidate = false;
    } else if (!user.cellPhone){
      isValidate = false;
    }else if (!user.email){
      isValidate = false;
    }
    return isValidate;
  }
  public saveOrEdit(user:UserModel): Observable<RestResponse>{
    return this.http.post<RestResponse>("http://localhost:8080/userSave" , JSON.stringify(user));
  }

}
